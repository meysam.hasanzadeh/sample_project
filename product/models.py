from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name='name')
    length = models.PositiveIntegerField(verbose_name='length', default=0)
    width = models.PositiveIntegerField(verbose_name='width', default=0)
    height = models.PositiveIntegerField(verbose_name='height', default=0)
    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='products', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Product'
        db_table = 'Product'
